# Home Server GitLab Executor

This repo contains the GitLab Kubernetes Executor used on my home server.

## Setup

### Create Namespace

```bash
kubectl create ns gitlab
```

### Add Helm Repo

```bash
helm repo add gitlab https://charts.gitlab.io
helm repo update gitlab
```
### Setup Chart Directory

```bash
cd chart
helm dependency update
```

### Get the Runner Token

In the `gitlab-terraform` repository, run the following commands:

```bash
cd gitlab
terraform output runner_token
```

You will now have the value of the runner token in the terminal output. Copy it (without the quotes) and add it to the `Gitlab Runner Token` entry in 1Password.

## Deploying

Run this command:

```bash
bash deploy.sh
```

This will install or upgrade the helm chart.