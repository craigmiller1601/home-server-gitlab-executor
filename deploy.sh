#!/bin/bash

set -eou pipefail

NAMESPACE=gitlab
CHART=gitlab-executor
ARGOCD_HOST=argocd.craigmiller160.us
ARGOCD_REPO_PATH="../craigmiller160-projects-argocd"
ARGOCD_LOCAL_PROJECT_PATH="./namespaces/$NAMESPACE/$CHART/project.yaml"
ARGOCD_PROJECT_PATH="$ARGOCD_REPO_PATH/$ARGOCD_LOCAL_PROJECT_PATH"

deploy_argocd() {
  helm template \
    -n "$NAMESPACE" \
    "$CHART" \
    ./chart \
    --values ./chart/values.yaml \
    > "$ARGOCD_PROJECT_PATH"

  (
    cd "$ARGOCD_REPO_PATH"
    git reset
    git pull
    git add "$ARGOCD_LOCAL_PROJECT_PATH"
    git commit -m "Updating $CHART"
    git push
  )

  argocd login \
    "$ARGOCD_HOST" \
    --grpc-web \
    --username "$ARGOCD_USERNAME" \
    --password "$ARGOCD_PASSWORD"

  argocd app \
    sync \
    "$CHART" \
    --grpc-web

  argocd app \
    wait \
    "$CHART" \
    --timeout 300 \
    --grpc-web
}

deploy_argocd